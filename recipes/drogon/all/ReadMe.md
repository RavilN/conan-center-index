How to build:

* Open terminal and navigate to the folder conan-center-index\recipes\drogon-conan\all

* Run commands (for Windows/Visual Studio 2019):

```
mkdir build
cd build
conan create ../ -pr=../VS2019_release -s compiler.cppstd=17 --build=jsoncpp --build=drogon
```

The drogon library should be built in the local conan cache.


* Ubuntu:

```
mkdir build-ubuntu
cd build-ubuntu
conan create ../ -s compiler.cppstd=17 --build=jsoncpp --build=drogon -pr=../ub-release-profile

