#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

pushd $SCRIPT_DIR

mkdir -p build-ubuntu
pushd build-ubuntu
conan create ../ -pr=../profile-ub1804 --build=missing
popd

popd
