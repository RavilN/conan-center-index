import os
import sys
from conans import ConanFile, CMake, tools
from conans.tools import download, unzip
from conans.model.version import Version
from conans.errors import ConanInvalidConfiguration
from urllib.parse import urlparse
import shutil

class restcCppConan(ConanFile):
    version = "0.10.0"
    requires = [
        ("openssl/1.1.1l"),
        ("rapidjson/1.1.0"),
        ("boost/1.79.0"),
        ("zlib/1.2.12")
        ]
    name = "restc-cpp"
    license = "MIT"
    exports_sources = [
        "CMakeLists.txt"
    ]
    homepage = "https://github.com/jgaa/restc-cpp"
    url = "https://github.com/conan-io/conan-center-index"
    description = "The magic that takes the pain out of accessing JSON API's from C++"
    topics = (
        "C++", "REST", "JSON"
    )
    settings = "os", "compiler", "build_type", "arch"
    options = {
        "shared": [False, True],
        "BOOST_LOG_STATIC_LINK": [True, False],
        "RESTC_CPP_WITH_EXAMPLES": [True, False],
        "RESTC_CPP_WITH_FUNCTIONALT_TESTS": [True, False],
        "RESTC_CPP_WITH_UNIT_TESTS": [True, False],
        "RESTC_CPP_LOG_WITH_BOOST_LOG": [True, False],
        "CMAKE_CXX_STANDARD": ["11", "14", "17"],
        "Boost_NO_SYSTEM_PATHS": [True, False],
        "RESTC_CPP_WITH_TLS": [True, False]
    }
    default_options = {
        "shared": False,
        "BOOST_LOG_STATIC_LINK": True,
        "RESTC_CPP_WITH_EXAMPLES": False,
        "RESTC_CPP_WITH_FUNCTIONALT_TESTS": False,
        "RESTC_CPP_WITH_UNIT_TESTS": False,
        "RESTC_CPP_LOG_WITH_BOOST_LOG": False,
        "CMAKE_CXX_STANDARD": "14",
        "Boost_NO_SYSTEM_PATHS": True,
        "RESTC_CPP_WITH_TLS": True
    }
    generators = "cmake", "cmake_find_package"

    cmake = None

    def configure(self):
        self.options["openssl"].shared = False
        self.options["zlib"].shared = False
        
        self.options["boost"].shared = False
        self.options["boost"].multithreading = True
        
        if self.settings.os == "Linux":
            self.settings.compiler.libcxx="libstdc++11"
        
    def requirements(self):
        pass
        
    @property
    def _source_subfolder(self):
        return "source_subfolder"

    def _patch_sources(self):
        #for patch in self.conan_data["patches"][self.version]:
        #    tools.patch(**patch)
        pass
        
    def config_options(self):
        pass
        
    def source(self):
        tools.rmdir("restc-cpp")
        gitUrl = self.conan_data["sources"][self.version]["url"]
        gitTag = self.conan_data["sources"][self.version]["tag"]
        if gitTag == "":
            self.run("git clone " + gitUrl)
        else:
            self.run("git clone " + "-b " + gitTag + " " + gitUrl)
        
        # Comment references to the external  rapidjson project
        tools.replace_in_file("restc-cpp/CMakeLists.txt", "include(cmake_scripts/external-projects.cmake)","#include(cmake_scripts/external-projects.cmake)")
        tools.replace_in_file("restc-cpp/CMakeLists.txt", "add_dependencies(${PROJECT_NAME} externalRapidJson)","#add_dependencies(${PROJECT_NAME} externalRapidJson)")

    def _configure_cmake(self):
        if self.cmake:
            return self.cmake
        self.cmake = CMake(self)
        self.cmake.verbose = True
        version = Version(self.version)

        if self.options["BOOST_LOG_STATIC_LINK"]:
            self.cmake.definitions["BOOST_LOG_STATIC_LINK"] = "ON"
        else:
            self.cmake.definitions["BOOST_LOG_STATIC_LINK"] = "OFF"

        if self.options["RESTC_CPP_WITH_EXAMPLES"]:
            self.cmake.definitions["RESTC_CPP_WITH_EXAMPLES"] = "ON"
        else:
            self.cmake.definitions["RESTC_CPP_WITH_EXAMPLES"] = "OFF"
                    
        if self.options["RESTC_CPP_WITH_FUNCTIONALT_TESTS"]:
            self.cmake.definitions["RESTC_CPP_WITH_FUNCTIONALT_TESTS"] = "ON"
        else:
            self.cmake.definitions["RESTC_CPP_WITH_FUNCTIONALT_TESTS"] = "OFF"
                    
        if self.options["RESTC_CPP_WITH_UNIT_TESTS"]:
            self.cmake.definitions["RESTC_CPP_WITH_UNIT_TESTS"] = "ON"
        else:
            self.cmake.definitions["RESTC_CPP_WITH_UNIT_TESTS"] = "OFF"
                    
        if self.options["RESTC_CPP_LOG_WITH_BOOST_LOG"]:
            self.cmake.definitions["RESTC_CPP_LOG_WITH_BOOST_LOG"] = "ON"
        else:
            self.cmake.definitions["RESTC_CPP_LOG_WITH_BOOST_LOG"] = "OFF"

        if self.options["Boost_NO_SYSTEM_PATHS"]:
            self.cmake.definitions["Boost_NO_SYSTEM_PATHS"] = "ON"
        else:
            self.cmake.definitions["Boost_NO_SYSTEM_PATHS"] = "OFF"

        if self.options["RESTC_CPP_WITH_TLS"]:
            self.cmake.definitions["RESTC_CPP_WITH_TLS"] = "ON"
        else:
            self.cmake.definitions["RESTC_CPP_WITH_TLS"] = "OFF"

        self.cmake.definitions["RESTC_CPP_WITH_TLS"] = "ON"
        
        self.cmake.definitions["CMAKE_CXX_STANDARD"]="14"

        if self.options["shared"]:
            self.cmake.definitions["BUILD_SHARED_LIBS"] = "ON"
        else:
            self.cmake.definitions["BUILD_SHARED_LIBS"] = "OFF"

        self.cmake.definitions["Boost_USE_MULTITHREADED"] = "ON"
        self.cmake.definitions["Boost_USE_STATIC_LIBS"] = "ON"
        
        self.cmake.definitions["INSTALL_RAPIDJSON_HEADERS"] = "OFF"
        
        self.cmake.configure()
        return self.cmake

    def build(self):
        self._patch_sources()
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        cmake = self._configure_cmake()
        cmake.install()

    def package_info(self):
        self.cpp_info.names["cmake_find_package"] = "restc-cpp"
        self.cpp_info.names["cmake_find_package_multi"] = "restc-cpp"
        self.cpp_info.libs = tools.collect_libs(self)
        self.cpp_info.includedirs = [
            "include",
            os.path.join("include", "restc-cpp"),
            os.path.join("include", "restc-cpp", "internals")
        ]
        if self.settings.os == "Windows":
            self.cpp_info.system_libs.append("ws2_32")
            self.cpp_info.includedirs.append(os.path.join("include", "win32"))
        else:
            self.cpp_info.includedirs.append(os.path.join("include", "posix"))
        self.cpp_info.builddirs = [
            "lib",
            os.path.join("lib", "cmake"),
            os.path.join("lib", "cmake", "restc-cpp")
        ]
