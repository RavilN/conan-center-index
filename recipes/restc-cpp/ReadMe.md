# Recipe to build restc-cpp with conan.


Work in progress. Currently, it uses a forked version of restc-cpp (https://gitlab.com/RavilN/restc-cpp), with some modified CMake files. With conan it is built as a sub-directory, and it was causing CMake errors due to the now different location of the root folder.

Location of the original project: https://github.com/jgaa/restc-cpp

The used version tag is still 0.10.0, although the used forked version is a few commits ahead.

## To build the project:

* Open the terminal and navigate to the folder ``conan-center-index\recipes\restc-cpp\all``
* Create a temporary build directory and cd to it
* Run command ``conan create ..``

It should pull dependencies from conan-center, clone restc-cpp source code, build it, and install the resulting library into conan local cache.
