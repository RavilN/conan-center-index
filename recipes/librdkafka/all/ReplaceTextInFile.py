#!/usr/bin/python

import sys, getopt

def main(argv):
  inputfile = ''
  outputfile = ''
  fromText = ''
  toText = ''
  try:
    opts, args = getopt.getopt(argv,"hf:t:i:o:",["from=","to=","ifile=","ofile="])
  except getopt.GetoptError:
    print ("Usage: ReplaceTextInFile.py -f <originalText> -t <replaceToText> -i <inputfile> -o <outputfile>")
    sys.exit(2)
  for opt, arg in opts:
    if opt == '-h':
      print ("Usage: ReplaceTextInFile.py -f <originalText> -t <replaceToText> -i <inputfile> -o <outputfile>")
      sys.exit()
    elif opt in ("-f", "--from"):
      fromText = arg
    elif opt in ("-t", "--to"):
      toText = arg
    elif opt in ("-i", "--ifile"):
      inputfile = arg
    elif opt in ("-o", "--ofile"):
      outputfile = arg

  if outputfile == '':
    outputfile = inputfile

  if len(fromText) == 0 or len(inputfile) == 0:
    print("Invalid arguments")
    print ("Usage: ReplaceTextInFile.py -f <originalText> -t <replaceToText> -i <inputfile> -o <outputfile>")
    sys.exit(2)

  print ("Changing text from [{}] to [{}] in file [{}] and writing the result to file [{}]".format(fromText, toText, inputfile, outputfile))
  t = ""
  with open(inputfile, "r") as reader:
    try:
      t = reader.read()
    except:
      print("Failed to read file {}".format(inputfile))
      sys.exit(2)

  t = t.replace(fromText, toText)

  with open(outputfile, "w") as writer:
    writer.write(t)
    print ("Done, changed text from [{}] to [{}] in file [{}] and wrote the result to file [{}]".format(fromText, toText, inputfile, outputfile))                                                                                    

if __name__ == "__main__":
  main(sys.argv[1:])